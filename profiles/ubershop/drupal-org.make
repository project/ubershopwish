; test make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[admin_menu][version] = "3.0-rc6"
projects[admin_menu][subdir] = "contrib"

projects[ctools][version] = "1.15"
projects[ctools][subdir] = "contrib"

projects[date][version] = "2.10"
projects[date][subdir] = "contrib"

projects[profiler_builder][version] = "1.0"
projects[profiler_builder][subdir] = "contrib"

projects[maxlength][version] = "3.3"
projects[maxlength][subdir] = "contrib"

projects[colorbox][version] = "2.13"
projects[colorbox][subdir] = "contrib"

projects[imageapi][version] = "1.x-dev"
projects[imageapi][subdir] = "contrib"

projects[l10n_client][version] = "1.3"
projects[l10n_client][subdir] = "contrib"

projects[l10n_update][version] = "2.4"
projects[l10n_update][subdir] = "contrib"

projects[potx][version] = "3.0"
projects[potx][subdir] = "contrib"

projects[entity_translation][version] = "1.1"
projects[entity_translation][subdir] = "contrib"

projects[i18n][version] = "1.27"
projects[i18n][subdir] = "contrib"

projects[rules][version] = "2.9"
projects[rules][subdir] = "contrib"

projects[background_process][version] = "1.17"
projects[background_process][subdir] = "contrib"

projects[backup_migrate][version] = "3.9"
projects[backup_migrate][subdir] = "contrib"

projects[entity][version] = "1.6"
projects[entity][subdir] = "contrib"

projects[libraries][version] = "2.5"
projects[libraries][subdir] = "contrib"

projects[logintoboggan][version] = "1.5"
projects[logintoboggan][subdir] = "contrib"

projects[pathauto][version] = "1.3"
projects[pathauto][subdir] = "contrib"

projects[progress][version] = "1.4"
projects[progress][subdir] = "contrib"

projects[token][version] = "1.6"
projects[token][subdir] = "contrib"

projects[transliteration][version] = "3.2"
projects[transliteration][subdir] = "contrib"

projects[rules][version] = "2.9"
projects[rules][subdir] = "contrib"

projects[ubercart][version] = "3.13"
projects[ubercart][subdir] = "contrib"

projects[ubercart][version] = "3.13"
projects[ubercart][subdir] = "contrib"

projects[ubercart][version] = "3.13"
projects[ubercart][subdir] = "contrib"

projects[uc_product_power_tools][version] = "1.1"
projects[uc_product_power_tools][subdir] = "contrib"

projects[uc_wishlist][version] = "2.0"
projects[uc_wishlist][subdir] = "contrib"

projects[wysiwyg][version] = "2.2"
projects[wysiwyg][subdir] = "contrib"

projects[wysiwyg_filter][version] = "1.6-rc2"
projects[wysiwyg_filter][subdir] = "contrib"

projects[variable][version] = "2.5"
projects[variable][subdir] = "contrib"

projects[views][version] = "3.23"
projects[views][subdir] = "contrib"

; +++++ Themes +++++

; mbase
projects[mbase][type] = "theme"
projects[mbase][version] = "1.0"
projects[mbase][subdir] = "contrib"

; +++++ Libraries +++++

; ColorBox
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][type] = "library"
libraries[colorbox][destination] = "libraries"
libraries[colorbox][download][type] = "get"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/master.zip"

; Profiler
libraries[profiler][directory_name] = "profiler"
libraries[profiler][type] = "library"
libraries[profiler][destination] = "libraries"
libraries[profiler][download][type] = "get"
libraries[profiler][download][url] = "http://ftp.drupal.org/files/projects/profiler-7.x-2.x-dev.tar.gz"

