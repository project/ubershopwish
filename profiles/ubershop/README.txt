Ubershop Wishlist is a Drupal distribution that integrates the functionality of the opensource Ubercart cart with the addition of the wish list functionality.
<p>Ubercart leverages the advantages of Drupal's major core and contributed systems, providing your users with shopping cart functionality that integrates with other parts of your company or community web site. It can be used to sell shippable goods, downloadable products, recurring memberships, and event tickets, and to enable complex interactions with Drupal through various add-on contributions.


=============================
What is Ubercart?
=============================
Ubercart is the most popular Drupal E-Commerce platform for your website. It implements everything you need to start selling products online.
Ubercart is not a standalone application, but it's a module for Drupal. This means that you first have to install and customize Drupal, and then install Ubercart and all its related third-party modules. Imagine how powerful this combination is. You won't have just a selling point for your products, but a complete platform to interact with your clients and market your company.
Solution for online shops with Drupal are:
<ul>
  <li>Integrated payment system to integrate between various payment methods and known payment gateways</li>
  <li>Transparent integration with Drupal basic core resulting in advanced reporting, user administration, and community-building features</li>
  <li>Batch import/export of products in the product repository</li>
  <li>Out-of-the-box functionality for each key aspect of your online store such as products, checkouts, payments, orders, and shipments</li>
  <li>Fully extensible with community wide support as it implements Drupal's core API programming patterns</li>
  <li>Activity logging and auditing</li>
</ul>
and more...


=============================
What include Ubershop Wishlist profile
=============================
Installation profiles provide site features and functions for a specific type of site as a single download containing Drupal core, contributed modules, themes, and pre-defined configuration.
Configure your store as you want thanks to the modules already included in the distribution such as:
&nbsp;
<ol>
  <li>multilingual con i18n</li>
  <li>image management with&nbsp;colorbox and imageapi</li>
  <li>translation updates with&nbsp;l10n_update e entity_translation</li>
  <li>automatic backup with backup_migrate</li>
  <li>optimization with background_process</li>
  <li>customization of ubercart with&nbsp;uc_product_power_tools</li>
</ol>


=============================
Libraries of this distribution
=============================
Colorbox
Colorbox is a light-weight customizable lightbox plugin for jQuery. This module allows for integration of Colorbox into Drupal.
Profiler
Profiler provides a new way to write install profiles. Gone are the days where you needed to know all the quirks of Drupal's APIs in order to write a solid install profile. 
&nbsp;


=============================
Modules activate in this distribution
=============================
The best and most useful modules are installed for an optimal construction of your shop.
&nbsp;

  admin_menu
  ctools
  date
  maxlength
  colorbox
  imageapi
  l10n_client
  l10n_update
  potx
  entity_translation
  i18n
  rules
  background_process
  backup_migrate
  entity
  libraries
  logintoboggan
  pathauto
  progress
  token
  transliteration
  rules
  ubercart
  uc_product_power_tools
  uc_wishlist
  variable
  views

=============================
Theme included
=============================

  mbase
  simplenav
  restaurant_table

The ubershop profile installs and activates the modules for managing the shop, activates the restaurant: table theme and provides a theme based on bootstrap 3 to be activated if desired.
To customize graphics, products, contents and pages, it is necessary to know how Drupal works and its characteristics.
