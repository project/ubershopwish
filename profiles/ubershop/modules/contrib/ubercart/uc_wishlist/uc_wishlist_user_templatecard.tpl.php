<?php

/**
 * @file
 * Default theme implementation to display a wishlist in a card.
 * 
 * @vars 
 * terms_item, expired, expiration_date
 */
$sign_after = variable_get('uc_sign_after_amount', FALSE);
$prec = variable_get('uc_currency_prec', 2);
$sign = variable_get('uc_currency_sign', '$');
$thou = variable_get('uc_currency_thou', ',');
$dec = variable_get('uc_currency_dec', '.');
				   
?>
<style>
.wishlist-products-qty{
  font-size: 17px;
  color: #000000;
  font-weight: 900;
  margin-top: 100%;
  justify-content: center;
  display: flex;
}
.wishlist-products{
  background-color: #f7f7f7;
}
.sell-price{
  color: #989595;
}
.card .card-header{
  border:none !mportant;
}
</style>
 <div class="row wishlist-products">
 <?php foreach ($terms_item as $key => $value): ?>
 <div class="col-1">
  <div class="wishlist-products-qty"><?php print $value['wishlist_products_qty']; ?>x</div>
 </div>
 <div class="col-11">
	<div class="card">
	<h5 class="card-header">
		<a data-toggle="collapse" href="#collapse-card-<?php print $value['nid']; ?>" aria-expanded="true" aria-controls="collapse-card-<?php print $value['nid']; ?>" id="heading-card-<?php print $value['nid']; ?>" class="d-block">
			<i class="fa fa-chevron-down pull-right"></i>
			<strong><?php print $key; ?></strong>
		</a>
	</h5>
	<div id="collapse-card-<?php print $value['nid']; ?>" class="collapse show" aria-labelledby="heading-card-<?php print $value['nid']; ?>">
		<div class="card-body">
		<?php print $value['node_title']; ?>
		<p><span><a href="#" title="<?php print ('Product added yet'); ?>"><i class="fa fa-heart"></i> </a></span>
		<span class="float-right sell-price"><?php print isset($value['sell_price']) ? number_format($value['sell_price'], $prec, $dec, $thou) : ''; ?> <?php print $sign; ?></span>
		</p>
	</div>
	</div>
	</div> 
 </div>	
 <?php endforeach; ?>
</div>
