/**
 * Sets the behavior to (un)collapse the cart block on a click
 */
Drupal.behaviors.ucWidgetIncrease = {
  attach: function(context) {
	  
	var count = 1;  
	var countEl = document.getElementById("qtywidget");
	
	var inc = document.getElementById('edit-increase');	
	inc.addEventListener('click', function() {
		if (count < 99) {
        count++;
        countEl.value = count;
		}

    }, false);
	
	var dec = document.getElementById('edit-decrease'); 
	dec.addEventListener('click', function() {
        if (count > 1) {
        count--;
        countEl.value = count;
      }  

    }, false);
	
  }
}